
var app = require('../app');
var debug = require('debug')('gestion-de-chambre-hotel:server');
var https = require('https');
var path = require('path');
var fs = require('fs');



 // Récuperer du port et l'enregistrer dans express
 

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);


 //creation HTTPS server.
 

var server = https.createServer(
  {
    key: fs.readFileSync(path.join(__dirname, 'certificate', 'key.pem')),

    cert: fs.readFileSync(path.join(__dirname, 'certificate', 'cert.pem')),

  },
  app);


 // l'écoute du port et la normalisation


server.listen(port, () => console.log('Secure Server on port', port));
server.on('error', onError);
server.on('listening', onListening);


function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    
    return val;
  }

  if (port >= 0) {
    
    return port;
  }

  return false;
}

// un Event listener en cas d'erreur du HTTPS server
 
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}


function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
