const express = require("express");
const controller = require("../controllers/reservation.controller");
const { authJwt } = require("../middlewares");
const { isAdmin } = require("../middlewares/authJwt");

var router = express.Router();

router.all("/",(req, res, next) => {
    res.statusCode = 200;
    res.setHeader(
        "Access-Control-Allow-Headers",
        "token, Origin, Content-Type, Accept"
        );
    next();
})

router.get("/",[authJwt.verifyToken, authJwt.isAdmin], (req, res, next) => {console.log("Accès reservation");next()},controller.getAll);

router.post("/",[authJwt.verifyToken],controller.addOne);

 router.get("/:id",[authJwt.verifyToken],controller.getOne);

 router.put("/:id",[authJwt.verifyToken],controller.updateOne);

 router.delete("/:id",[authJwt.verifyToken],controller.deleteOne);

 module.exports = router;
