const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");
const { isAdmin } = require("../middlewares/authJwt");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "token, Origin, Content-Type, Accept"
    );
    next();
  });

 
  app.get("/test/user/:id",[authJwt.verifyToken, authJwt.isUser ], controller.userDetails);

  app.put("/test/user/:id",[authJwt.verifyToken, authJwt.isUser], controller.updateDetailsUser );

  app.delete("/test/user/:id",[authJwt.verifyToken],controller.deleteAccount);

  app.put("/test/admin/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.updateDetailsAdmin );

  app.get( "/test/admin", [authJwt.verifyToken, authJwt.isAdmin], controller.usersList );

};
