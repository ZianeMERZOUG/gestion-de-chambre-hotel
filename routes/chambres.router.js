const express = require("express");
const controller = require("../controllers/chambres.controller");
const { authJwt } = require("../middlewares");

const { isAdmin } = require("../middlewares/authJwt");
var router = express.Router();


router.all("/",(req, res, next) => {
    res.statusCode = 200;
    res.setHeader(
        "Access-Control-Allow-Headers",
        "token, Origin, Content-Type, Accept"
    );
    next();
})
router.get("/", (req, res, next) => {console.log("coucou");next()},controller.getAll);
router.post("/",[authJwt.verifyToken, authJwt.isAdmin],controller.addOne);


 router.get("/:id",controller.getOne)
 router.put("/:id",[authJwt.verifyToken, authJwt.isAdmin],controller.updateOne);
 router.delete("/:id",[authJwt.verifyToken, authJwt.isAdmin],controller.deleteOne);



 module.exports = router;


 