const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReservationSchema = new Schema(
{
numero_de_reservation: { type: Number},
numero_de_chambre: {type: Number},
nombre_de_lit: { type: Number},
nom_client: { type: String},
prenom_client: { type: String},
nombre_personnes: { types: Number},
duree_reservation: { type: Number },
date_debut_reservation: { type: String},
heure_reservation: { type: Number },

},

{ collection: "reservations", timestamps: true}
);

module.exports= mongoose.model("reservation", ReservationSchema);