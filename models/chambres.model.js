const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChambreSchema = new Schema(
{
numero_de_chambre: { type: Number},
prix: { type: Number},
nombre_de_lit: { type: Number},
superficie: { type: Number},
description: { type: String},
image: { types: String},
disponibilité: { type: Boolean},
},

{ collection: "chambres", timestamps: true}
);

module.exports= mongoose.model("chambre", ChambreSchema);
