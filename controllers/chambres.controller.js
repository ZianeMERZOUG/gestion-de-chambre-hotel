const Chambre = require("../models/chambres.model");
let controller={

// Afficher toutes les chambres  ;soit par l'admin ou un simple user
    getAll: async (req, res, next) => {

    let chambres = await Chambre.find({});res.json(chambres);},

//Ajouter une chambre; doit se faire par l'admin
    addOne: async (req, res, next) => {
        let chambre = await Chambre.create(req.body)
        res.json(chambre);},


//Afficher une chambre en la recherchant par son id  ;soit par l'admin ou un simple user
    getOne: async (req, res, next) => {
        let chambre = await Chambre.findById(req.params.id);
        res.json(chambre);},

//Modifier les informations d'une chambre ; doit se faire par l'admin  
    updateOne: async (req, res, next) => {
        let chambre = await Chambre.findByIdAndUpdate(req.params.id, req.body,{ new:true });
        res.json(chambre);},

//Supprimer une chambre definitivement du serveur  ; doit se faire par l'admin    
    deleteOne: async (req, res, next) => {
        let resp = await Chambre.findByIdAndRemove(req.params.id);
        res.json(resp);},

 

   };
   
   module.exports = controller;
