const reservation = require("../models/reservation.model");
let controller={
    
    // Afficher toutes les résérvation pour l'admin
    getAll: async (req, res, next) => {

        let reservations = await reservation.find({});res.json(reservations);
        
        },
    // un utilisateur ou un admin peut ajouter une réservation
    addOne: async (req, res, next) => {
        let reservations = await reservation.create(req.body)
        res.json(reservations);},

    // un utilisateur ou un admin peut affiché une réservation
    getOne: async (req, res, next) => {
        let reservations = await reservation.findById(req.params.id);
        res.json(reservations);},

    // un utilisateur ou un admin peut modifier une réservation
    updateOne: async (req, res, next) => {
        let reservations = await reservation.findByIdAndUpdate(req.params.id, req.body,{ new:true });
        res.json(reservations);},
        
    // un utilisateur ou un admin peut supprimer une réservation
    deleteOne: async (req, res, next) => {
        let resp = await reservation.findByIdAndRemove(req.params.id);
        res.json(reservations);},
   };
   
   module.exports = controller;
