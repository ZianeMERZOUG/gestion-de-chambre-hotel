const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");

var simplecrypt = require('simplecrypt');
var sc = simplecrypt();


// Inscrition d'un nouvel utilisateur avec la fonction signup 
// Cette fonction prend en paramettre les champs du model de donnée user
// Si le role n'est pas spécifié; user sera le role par default

exports.signup = (req, res) => {
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: sc.encrypt(req.body.password)
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role._id);
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save(err => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};



// Authentification d'un utilisateur avec la fonction signin qui prend en paramétres le username et password

exports.signin = (req, res) => {
  User.findOne({
    username: req.body.username,
    
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }
      
      var password = req.body.password;
      var passwordIsValid = ( (sc.encrypt(password) == user.password))
 
      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }
      
     var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: "1 hours" // 24 hours
      });


 // Retourner les information de l'utilisateur et son Token Access pour l'utiliser pour l'accès

        var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      res.status(200).send({
        id: user._id,
        username: user.username,
        email: user.email,
        roles: authorities,
        accessToken: token
      });
    });
};
