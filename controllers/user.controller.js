
//un utilisateur affiche ces informations par son ID
const User = require("../models/user.model");
exports.userDetails = async(req, res,next) => {
  let users = await User.findById(req.params.id);
  res.json(users);
};


//un utilisateur peut modifier ses informations de son compte
exports.updateDetailsUser = async(req, res,next) => {
  let users = await User.findByIdAndUpdate(req.params.id, req.body,{ new:true });
  res.json(users);
};

//un adminisatrateur peut modifier les informations de son compte
exports.updateDetailsAdmin = async(req, res,next) => {
  let users = await User.findByIdAndUpdate(req.params.id, req.body,{ new:true });
  res.json(users);
};

//un utilisateur peut supprimer son compte
exports.deleteAccount = async(req, res,next) => {
  let resp = await User.findByIdAndRemove(req.params.id);
  res.json(resp);

};


//un admin peut voir la liste de tous les utilisateurs
exports.usersList = async(req, res,next) => {
  let users = await User.find({});res.json(users);
};


