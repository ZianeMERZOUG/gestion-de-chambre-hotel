#Gestion de chambre d'hotel
un projet etudiant qui  consiste a realiser un API pour la reservation de chambre dans un hotel.


##Recuperation du projet
Vous allez récupérer les fichiers nécessaires au travail à l'aide de Git, saisissez la commande:
$ git clone https://gitlab.com/ZianeMERZOUG/gestion-de-chambre-hotel

##Outils
Cet API a été développé en nodejs et a besoin d'une base de données mongodb pour fonctionner
il faut créer dans mongodb, une base de données chambre_db et creer les collections: reservations,chambres,users et roles. 
pour le nom de la base de données, il est possible de le modifier dans le fichier db.config


##Installation 
Dans cette partie, nous evoquons les differentes installations necessaires pour le bon fonctionnement du projet
$ npm install
$ npm start
$ npm install -g express-generator 
$ npm  install nodemon --save-dev
$ npm start nodemon 
$ npm install mongoose
$ npm install  body-parser cors jsonwebtoken --save


##Lancement et test 
pour le lancement du projet il faut faire un:
npm run start:local
Les  getAll peuvent etre testées via le navigateur.
pour les autres fonctionnalités , il faut un outil du genre postman.

###Auteurs
ce projet a été dévéloppé par deux etudiants Aicha SIDIBE et 
Ziane MERZOUG en Master 1 Miage a l'université Paris Descartes 

###LICENCE



